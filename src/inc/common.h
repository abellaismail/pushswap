/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   common.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: iait-bel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/18 11:22:49 by iait-bel          #+#    #+#             */
/*   Updated: 2022/02/19 09:16:40 by iait-bel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COMMON_H
# define COMMON_H

typedef struct s_item
{
	int				val;
	unsigned int	sort_pos;
}	t_item;

typedef struct s_stack
{
	unsigned int	len;
	t_item			*items;
}	t_stack;

typedef struct s_data
{
	int		print_inst;
	int		size;
	t_stack	sta;
	t_stack	stb;
}	t_data;

void	swap(t_item *arr, int i1, int i2);
int		swapa(t_data *data);
int		swapb(t_data *data);
int		swapab(t_data *data);

int		pusha(t_data *data);
int		pushb(t_data *data);

int		swipeupa(t_data *data);
int		swipeupb(t_data *data);
int		swipeup(t_data *data);

int		swipedowna(t_data *data);
int		swipedownb(t_data *data);
int		swipedown(t_data *data);

int		instra_s(t_stack *st);
int		instra_p(t_stack *st1, t_stack *st2);
int		instra_r(t_stack *st);
int		instra_rr(t_stack *st);

void	ft_pushswap(t_data *data);
t_stack	stack_wrap(t_item *items, int cap);
t_item	*av2int(char **av, int size);

int		ft_strlen(char *str);
int		ft_strcmp(char *s1, char *s2);
void	ft_putstrfd(int fd, char *str);

#endif
