/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: iait-bel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/18 11:28:34 by iait-bel          #+#    #+#             */
/*   Updated: 2022/02/19 08:46:26 by iait-bel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "common.h"
#include <stdlib.h>

t_stack	stack_wrap(t_item *items, int cap)
{
	t_stack	st;

	st.items = items;
	st.len = cap;
	return (st);
}

int	has_dup(t_stack *st)
{
	unsigned int	i;
	unsigned int	j;

	i = 0;
	while (i < st->len)
	{
		j = i + 1;
		while (j < st->len)
		{
			if (st->items[i].val == st->items[j].val)
				return (1);
			j++;
		}
		i++;
	}
	return (0);
}

int	data_setup(int size, char **av)
{
	t_data	data;

	data.print_inst = 1;
	data.sta = stack_wrap(av2int(av, size), size);
	data.stb = stack_wrap(malloc(sizeof(t_item) * size), 0);
	if (data.sta.items == 0 || data.stb.items == 0 || has_dup(&data.sta))
	{
		free(data.sta.items);
		free(data.stb.items);
		ft_putstrfd(2, "ERROR\n");
		return (1);
	}
	ft_pushswap(&data);
	free(data.sta.items);
	free(data.stb.items);
	return (0);
}

int	main(int ac, char **av)
{
	if (ac == 1)
		return (0);
	return (data_setup(ac - 1, av + 1));
}
