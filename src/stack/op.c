/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   op.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: iait-bel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/18 11:18:04 by iait-bel          #+#    #+#             */
/*   Updated: 2022/02/18 11:18:07 by iait-bel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"
#include "stack.h"

void	push(t_stack *st, t_item *item)
{
	st->items[st->len].val = item->val;
	st->items[st->len].sort_pos = item->sort_pos;
	st->len++;
}

t_item	*pop(t_stack *st)
{
	if (st->len == 0)
	{
		return (0);
	}
	st->len--;
	return (st->items + st->len);
}
