/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swipedown.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: iait-bel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/18 11:21:10 by iait-bel          #+#    #+#             */
/*   Updated: 2022/02/18 11:21:48 by iait-bel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"
#include "common.h"

int	swipedowna(t_data *data)
{
	if (data->print_inst)
		ft_putstrfd(1, "rra\n");
	return (instra_rr(&data->sta));
}

int	swipedownb(t_data *data)
{
	if (data->print_inst)
		ft_putstrfd(1, "rrb\n");
	return (instra_rr(&data->stb));
}

int	swipedown(t_data *data)
{
	if (data->print_inst)
		ft_putstrfd(1, "rrr\n");
	return (instra_rr(&data->sta) || instra_rr(&data->stb));
}
