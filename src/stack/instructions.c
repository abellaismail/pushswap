/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instructions.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: iait-bel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/18 11:22:08 by iait-bel          #+#    #+#             */
/*   Updated: 2022/02/18 11:22:09 by iait-bel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"
#include "pushswap.h"

int	instra_s(t_stack *st)
{
	if (st->len <= 1)
		return (0);
	swap(st->items, st->len -1, st->len - 2);
	return (1);
}

int	instra_p(t_stack *st1, t_stack *st2)
{
	t_item	*item;

	if (st2->len == 0)
		return (0);
	item = pop(st2);
	push(st1, item);
	return (1);
}

int	instra_r(t_stack *st)
{
	int	len;

	len = st->len;
	if (len == 0)
		return (0);
	if (st->len == 1)
		return (1);
	while (len-- > 1)
		swap(st->items, len, len - 1);
	return (1);
}

int	instra_rr(t_stack *st)
{
	unsigned int	i;

	i = 0;
	if (st->len == 0)
		return (0);
	if (st->len == 1)
		return (1);
	while (i < st->len -1)
	{
		swap(st->items, i, i + 1);
		i++;
	}
	return (1);
}
