/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swipeup.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: iait-bel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/18 11:20:24 by iait-bel          #+#    #+#             */
/*   Updated: 2022/02/18 11:21:01 by iait-bel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"
#include "stack.h"

int	swipeupa(t_data *data)
{
	if (data->print_inst)
		ft_putstrfd(1, "ra\n");
	return (instra_r(&data->sta));
}

int	swipeupb(t_data *data)
{
	if (data->print_inst)
		ft_putstrfd(1, "rb\n");
	return (instra_r(&data->stb));
}

int	swipeup(t_data *data)
{
	if (data->print_inst)
		ft_putstrfd(1, "rr\n");
	return (instra_r(&data->sta) && instra_r(&data->stb));
}
