/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: iait-bel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/18 11:13:58 by iait-bel          #+#    #+#             */
/*   Updated: 2022/02/19 11:11:52 by iait-bel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"
#include "common.h"
#include <stdlib.h>

int	is_sorted(t_stack st)
{
	unsigned int	i;

	i = 1;
	while (i < st.len)
	{
		if (st.items[i].sort_pos != i)
			return (0);
		i++;
	}
	return (1);
}

void	ft_pushswap(t_data *data)
{
	int	n;

	data->print_inst = 0;
	while (1)
	{
		n = run_instra(data);
		if (n == 0
			&& is_sorted(data->sta)
			&& data->stb.len == 0)
		{
			ft_putstrfd(1, "OK\n");
			break ;
		}
		else if (n == 0)
		{
			ft_putstrfd(2, "KO\n");
			break ;
		}
		else if (n == -1)
		{
			ft_putstrfd(2, "Error\n");
			return ;
		}
	}
}
