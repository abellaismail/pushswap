/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_sort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: iait-bel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/18 11:13:01 by iait-bel          #+#    #+#             */
/*   Updated: 2022/02/18 13:17:32 by iait-bel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"
#include "common.h"

int	_swipe_up(t_data *data, int sbu)
{
	if (sbu == 1)
		swipeup(data);
	else
		swipeupa(data);
	return (0);
}

void	push2b(t_data *data, int div)
{
	int				max_pos;
	unsigned int	count;
	int				sbu;
	int				pos;

	div = data->sta.len / div;
	max_pos = data->sta.len - div;
	count = 0;
	sbu = 0;
	while (data->sta.len)
	{
		pos = data->sta.items[data->sta.len - 1].sort_pos;
		if (pos >= max_pos)
		{
			if (sbu == 1)
				swipeupb(data);
			pushb(data);
			sbu = pos >= max_pos + div / 2;
			count++;
			if (count % div == 0)
				max_pos -= div;
		}
		else
			sbu = _swipe_up(data, sbu);
	}
}

void	bring_push_a(t_data *data, int pos)
{
	bring2top_b(&data->stb, pos);
	pusha(data);
}

void	back2a(t_data *data)
{
	int		i;
	t_stack	*stb;
	t_pos	pos;

	stb = &data->stb;
	i = 0;
	while (stb->len)
	{
		find_mins_by_pos(stb, &pos, i);
		if (dis(stb, pos.cur) <= dis(stb, pos.next) || pos.next == -1)
			bring_push_a(data, pos.cur);
		else
		{
			bring_push_a(data, pos.next);
			find_mins_by_pos(stb, &pos, i);
			bring_push_a(data, pos.cur);
			swapa(data);
			i++;
		}
		i++;
	}
}

int	sort_on_stack(t_data *data, int div)
{
	push2b(data, div);
	back2a(data);
	return (1);
}
