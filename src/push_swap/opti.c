/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   opti.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: iait-bel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/18 11:11:47 by iait-bel          #+#    #+#             */
/*   Updated: 2022/02/18 13:17:12 by iait-bel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"
#include "stack.h"
#include "common.h"

void	bring2top_a(t_stack *st, unsigned int pos)
{
	int	mv;

	if (pos == st->len - 1)
		return ;
	if (pos >= st->len / 2)
	{
		mv = st->len - pos - 1;
		while (mv--)
		{
			ft_putstrfd(1, "ra\n");
			instra_r(st);
		}
	}
	else
	{
		mv = pos + 1;
		while (mv--)
		{
			ft_putstrfd(1, "rra\n");
			instra_rr(st);
		}
	}
}

void	bring2top_b(t_stack *st, unsigned int pos)
{
	int	mv;

	if (pos == st->len - 1)
		return ;
	if (pos >= st->len / 2)
	{
		mv = st->len - pos - 1;
		while (mv--)
		{
			ft_putstrfd(1, "rb\n");
			instra_r(st);
		}
	}
	else
	{
		mv = pos + 1;
		while (mv--)
		{
			ft_putstrfd(1, "rrb\n");
			instra_rr(st);
		}
	}
}
