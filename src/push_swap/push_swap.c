/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: iait-bel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/18 11:11:56 by iait-bel          #+#    #+#             */
/*   Updated: 2022/02/18 11:32:12 by iait-bel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"
#include "stack.h"
#include <stdlib.h>

void	ft_pushswap(t_data *data)
{
	if (data->sta.len <= 2)
	{
		if (data->sta.items[0].sort_pos == 1)
			swapa(data);
	}
	else if (data->sta.len <= 5)
		nano_sort(data);
	else if (data->sta.len < 50)
		sort_on_stack(data, 4);
	else if (data->sta.len < 200)
		sort_on_stack(data, 5);
	else
		sort_on_stack(data, 9);
}
