/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_5.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: iait-bel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/18 11:12:07 by iait-bel          #+#    #+#             */
/*   Updated: 2022/02/18 11:12:08 by iait-bel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"
#include "pushswap.h"

void	bring_push_b(t_data *data, int pos)
{
	bring2top_a(&data->sta, pos);
	pushb(data);
}

void	sort_5(t_data *data)
{
	t_stack	*st;
	t_pos	pos;

	st = &data->sta;
	find_mins_by_pos(st, &pos, 4);
	if (dis(&data->stb, pos.cur) <= dis(&data->stb, pos.prev))
		bring_push_b(data, pos.cur);
	else
	{
		bring_push_b(data, pos.prev);
		find_mins_by_pos(st, &pos, 4);
		bring_push_b(data, pos.cur);
		swapb(data);
	}
}

void	sort_4(t_data *data)
{
	t_stack	*st;
	t_pos	pos;

	st = &data->sta;
	find_mins_by_pos(st, &pos, 3);
	bring_push_b(data, pos.cur);
}

void	sort_3(t_data *data)
{
	t_stack	*st;

	st = &data->sta;
	if ((st->items[0].sort_pos == 2 && st->items[1].sort_pos == 1)
		|| (st->items[2].sort_pos == 2 && st->items[1].sort_pos == 0)
		|| (st->items[0].sort_pos == 0 && st->items[1].sort_pos == 2))
		swapa(data);
	if (st->items[1].sort_pos == 0 && st->items[2].sort_pos == 1)
		swipedowna(data);
	else if (st->items[0].sort_pos == 1 && st->items[1].sort_pos == 2)
		swipeupa(data);
}

void	nano_sort(t_data *data)
{
	if (data->sta.len == 5)
		sort_5(data);
	if (data->sta.len == 4)
		sort_4(data);
	sort_3(data);
	while (data->stb.len)
		pusha(data);
}
