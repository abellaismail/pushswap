/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pushswap.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: iait-bel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/18 11:23:30 by iait-bel          #+#    #+#             */
/*   Updated: 2022/02/19 09:17:10 by iait-bel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSHSWAP_H
# define PUSHSWAP_H

typedef struct s_pos
{
	int	prev;
	int	cur;
	int	next;
}	t_pos;

# include "common.h"
void	find_mins_by_pos(t_stack *st, t_pos *pos, unsigned int s_pos);
void	nano_sort(t_data *data);
int		sort_on_stack(t_data *data, int div);
int		min(int a, int b);
void	bring2top_a(t_stack *st, unsigned int pos);
void	bring2top_b(t_stack *st, unsigned int pos);
int		dis(t_stack *st, int pos);

#endif
