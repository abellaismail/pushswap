/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   misc.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: iait-bel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/18 11:12:43 by iait-bel          #+#    #+#             */
/*   Updated: 2022/02/18 11:12:46 by iait-bel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pushswap.h"

int	min(int a, int b)
{
	if (a > b)
		return (b);
	return (a);
}

int	dis(t_stack *st, int cur_pos)
{
	return (min(cur_pos, st->len - cur_pos));
}

void	find_mins_by_pos(t_stack *st, t_pos *pos, unsigned int s_pos)
{
	unsigned int	i;

	pos->prev = -1;
	pos->cur = -1;
	pos->next = -1;
	i = 0;
	while (i < st->len)
	{
		if (st->items[i].sort_pos == s_pos - 1)
			pos->prev = i;
		else if (st->items[i].sort_pos == s_pos)
			pos->cur = i;
		else if (st->items[i].sort_pos == s_pos + 1)
			pos->next = i;
		i++;
	}
}
