CC 				= cc
CCFLAGS 		= -Wall -Werror -Wextra -g
INCLUDE			= -Isrc/inc -Isrc/push_swap/inc
CHECKER_INC 	= -Isrc/inc -Isrc/checker/inc
 
B_DIR			= build

COMMON_FILES 	= main utils str stack/op stack/instructions stack/swap stack/swipeup stack/swipedown
C_OBJ 			= $(addprefix $(B_DIR)/, $(COMMON_FILES:=.o))

PUSH_SWAP_FILES	= push_swap opti stack_sort utils/misc sort_5
PUSH_SWAP_OBJ 	= $(addprefix $(B_DIR)/push_swap/, $(PUSH_SWAP_FILES:=.o))

CHECKER_FILES 	= checker exec 
CHECKER_OBJ 	= $(addprefix $(B_DIR)/checker/, $(CHECKER_FILES:=.o))

EXEC			= push_swap
CHECKER_EXEC	= checker 

all: $(EXEC)

$(EXEC): $(C_OBJ) $(PUSH_SWAP_OBJ)
	$(CC) $(CCFLAGS) -o $@ $^

$(B_DIR)/push_swap/%.o: src/push_swap/%.c src/push_swap/inc/pushswap.h
	mkdir -p $(@D)
	$(CC) $(CCFLAGS) $(INCLUDE) -o $@ -c $<

$(B_DIR)/checker/%.o: src/checker/%.c src/checker/inc/checker.h
	mkdir -p $(@D)
	$(CC) $(CCFLAGS) $(CHECKER_INC) -o $@ -c $<

$(B_DIR)/%.o: src/%.c src/inc/common.h
	mkdir -p $(@D)
	$(CC) $(CCFLAGS) $(INCLUDE) -o $@ -c $<

bonus: $(CHECKER_EXEC) 

$(CHECKER_EXEC): $(C_OBJ) $(CHECKER_OBJ)
	$(CC) $(CCFLAGS) -o $@ $^

clean:
	rm -f $(C_OBJ) $(PUSH_SWAP_OBJ)
	rm -rf $(B_DIR)

fclean: clean
	rm -f $(EXEC) $(CHECKER_EXEC)

re: fclean all

rebonus: fclean $(EXEC) $(CHECKER_EXEC)

.PHONY: all clean fclean re rebonus bonus 

