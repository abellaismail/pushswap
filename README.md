# push_swap
After 5 Epic Fails in 2 months trying to solve push_swap by myself I give up and I used [this algo](https://github.com/izenynn/push_swap/blob/main/README.md)

# My Touch
I tried to implement this solution mentioned above but I failed

So I tried to optimize it.

* **for numbers between 1-5**:
	I used the same algo.

* **for numbers between 6-INFINITY**:
	when I push from `stack a` to `b` I split every chunk into 2 chunks and I push the numbers that belongs to the first half to `stack b`
	for the second chunk I do the same then swipe up `stack b`.
	this will result in a beautiful shape in `stack b` that will look like the image bellow if you use PUSH_SWAP_VISUALIZER
	
![screenshot](screenshot.png)
